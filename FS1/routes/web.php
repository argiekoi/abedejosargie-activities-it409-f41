<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});






Route::prefix('api')->group(function (){

    // Get songs
    Route::get('songs', 'SongsController@index');

    Route::post('/uploadSongs/{title}/{length}/{artist}/{created_at}/{updated_at}', 'SongsController@uploadSong');

    Route::get('/playlists', 'PlaylistController@index');
    Route::post('/createPlaylist/{playlistName}', 'PlaylistController@createPlaylist');

    Route::get('/playlist_song', 'PlaylistController@showPlaylistSong');
    Route::get('/playlist_song/{playlistId}/{songId}', 'PlaylistController@addPlaylistSong');
    
});
