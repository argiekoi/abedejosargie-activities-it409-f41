<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PlaylistController extends Controller
{

    public function index(){
        $playlist = DB::select('select * from playlists');
        return ['playlist'=>$playlist];
    }

    public function createPlaylist($playlistName){
        $data=array('name'=>$playlistName,"created_at"=>date("Y-m-d h:i:sa"),"updated_at"=>date("Y-m-d h:i:sa"));
        DB::table('playlists')->insert($data);
        echo "Playlist inserted successfully.<br/>";
    }

    public function showPlaylistSong(){
        $playlist_songs = DB::select('select ps.id, title, artist, length from playlist_songs ps, songs s where s.id = ps.song_id');
        return ['playlist_songs'=>$playlist_songs];
    }
    public function addPlaylistSong($playlistId, $songId){
        $data=array('song_id'=>$songId,'playlist_id'=>$playlistId,"created_at"=>date("Y-m-d h:i:sa"),"updated_at"=>date("Y-m-d h:i:sa"));
        DB::table('playlist_songs')->insert($data);
        echo "Playlist song inserted successfully.<br/>";
    }
}
